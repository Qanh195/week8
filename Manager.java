package Quan_Ly;

public class Manager extends Employee {
	Employee assistant;
	public Manager() {
		super();
		assistant = new Employee();
	}

	public Manager(String name, date d, double s) {
		super(name, d, s);
		
		assistant = new Employee();
	}
	
	public void setAssistant(Employee input)
	{
		assistant = input;
	}
	
	public String toString()
	{
		String res = "Quan ly ten la : " + getName()+ "\n\t\tSalaries: " + getSalary() + "\n\t\tThis person has an employee name:" + assistant.getName() ;
		return res;
	}

}
