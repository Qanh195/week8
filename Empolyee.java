package Quan_Ly;

public class Employee extends person {
	private double salary;
	public Employee()
	{
		super();
		salary = 0.0;
	}
	public Employee(String name, int d, int m, int y, double s)
	{
		super(name, d, m, y);
		salary = s;
	}
	public Employee(String name, date d, double s)
	{
		super(name, d);
		salary = s;
	}
	
	public double getSalary()
	{
		return salary;
	}
	
	public String toString()
	{
		return " Nhan vien nay ten : " + getName()+ "\n\t\tSalaries: " + getSalary();
	}
}
