package Quan_Ly;

public class people {
	public static void main(String[] args)
	{
		Employee newbie = new Employee("Newbie", new date(25,2,1989), 1000000);
		
		Manager boss = new Manager("Boss", new date(23,2,1979), 4000000);
		boss.setAssistant(newbie);

		Manager bigBoss = new Manager("BigBoss", new date(3,12,1969), 10000000);
		bigBoss.setAssistant(boss);
		
		person[] array = new person[3];
		array[0] = newbie;
		array[1] = boss;
		array[2] = bigBoss;
		for(int i = 0; i<3; i++)
		{
			System.out.println(array[i].toString());
		}
	}
}
